from django.http import HttpRequest, HttpResponse, JsonResponse
from django.db.models import QuerySet
from .models import *


def get_channels(request: HttpRequest) -> JsonResponse:
    # return only the parent channels
    channels: QuerySet = Channel.objects.filter(parent=None)

    response = []
    for channel in channels:
        response.append({
            "id": channel.id,
            "title": channel.title,
            "language": channel.language,
            "picture": channel.picture.path,
            "group": [group.title for group in channel.group.all()],
            "created_at": channel.created_at,
            "updated_at": channel.updated_at
        })

    return JsonResponse(response, safe=False)


def get_contents(request: HttpRequest, channel_id: int) -> JsonResponse:
    channel: QuerySet = Channel.objects.filter(id=channel_id)
    response = []

    if (channel):
        contents: QuerySet = Content.objects.filter(channel_id=channel[0].id)

        for content in contents:
            response.append({
                "id": content.id,
                "file": content.file.path,
                "meta_data": [{
                    "author": data.author,
                    "genre": data.genre,
                    "content_description": data.content_description,
                    "rating": data.rating,
                } for data in MetaData.objects.filter(content_id=content.id)],
                "created_at": content.created_at,
                "updated_at": content.updated_at
            })

    return JsonResponse(response, safe=False)


def get_subchannels(request: HttpRequest, channel_id: int) -> JsonResponse:
    # return only the subchannels of given channel
    subchannels: QuerySet = Channel.objects.filter(parent_id=channel_id)

    response = []
    for channel in subchannels:
        response.append({
            "id": channel.id,
            "title": channel.title,
            "language": channel.language,
            "picture": channel.picture.path,
            "group": [group.title for group in channel.group.all()],
            "created_at": channel.created_at,
            "updated_at": channel.updated_at
        })

    return JsonResponse(response, safe=False)


def get_subchannel_contents(request, channel_id: int, subchannel_id: int) -> JsonResponse:
    subchannel: QuerySet = Channel.objects.filter(
        id=subchannel_id, parent_id=channel_id)
    response = []

    if (subchannel):
        contents: QuerySet = Content.objects.filter(
            channel_id=subchannel[0].id)

        for content in contents:
            response.append({
                "id": content.id,
                "file": content.file.path,
                "meta_data": [{
                    "author": data.author,
                    "genre": data.genre,
                    "content_description": data.content_description,
                    "rating": data.rating,
                } for data in MetaData.objects.filter(content_id=content.id)],
                "created_at": content.created_at,
                "updated_at": content.updated_at
            })

    return JsonResponse(response, safe=False)
