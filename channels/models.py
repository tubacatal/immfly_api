import decimal
from django.db import models
from django.db.models import Q, F
from django.utils.translation import gettext_lazy as _
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db.models.signals import m2m_changed
from django.dispatch import receiver


class Group(models.Model):
    title = models.CharField(max_length=200)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class Channel(models.Model):

    class Languages(models.TextChoices):
        ES = 'Spanish'
        TR = 'Turkish'
        EN = 'English'
        FR = 'French'
        PT = 'Portuguses'

    title = models.CharField(max_length=200)
    language = models.CharField(max_length=20, choices=Languages.choices)
    picture = models.ImageField(upload_to='contents/')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    # define relations
    parent = models.ForeignKey(
        'self', on_delete=models.SET_NULL, blank=True, null=True, related_name='subchannel')
    group = models.ManyToManyField(Group, related_name='group')

    def save(self, *args, **kwargs):
        try:
            # do not allow nested subchannels
            if (self.id and
                self.parent is not None and
                    self.parent.parent is not None):
                pass
            else:
                super(Channel, self).save(*args, **kwargs)
        except Exception as err:
            print(err)


class Content(models.Model):
    file = models.FileField(upload_to='contents/')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    # define relations
    channel = models.ForeignKey(Channel, on_delete=models.CASCADE)


class MetaData(models.Model):
    author = models.CharField(max_length=200)
    genre = models.CharField(max_length=50)
    content_description = models.TextField()
    rating = models.DecimalField(max_digits=2,
                                 decimal_places=1,
                                 validators=[
                                     MinValueValidator(decimal.Decimal(1)),
                                     MaxValueValidator(decimal.Decimal(10))
                                 ])
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    # define relations
    content = models.ForeignKey(Content, on_delete=models.CASCADE)


@receiver(m2m_changed, sender=Channel.group.through)
def subchannel_group_changed(sender, instance, action, *args, **kwargs):
    try:
        if (instance.parent is not None):
            subchannel_groups = instance.group.all()
            parent_groups = instance.parent.group.all().values_list('id')

            for group in subchannel_groups:
                if (action is "post_add"):
                    if group.id not in parent_groups:
                        instance.parent.group.add(group)
    except Exception as err:
        print(err)
