import csv
import time
from django.core.management.base import BaseCommand
from channels.models import *

fieldnames = ["Channel Title", "Average Rating"]


class Command(BaseCommand):
    help = 'Calculates ratings of all channels ands exports them as csv'

    def handle(self, *args, **kwargs):
        channels = Channel.objects.filter(parent_id=None)
        ratings = []

        for channel in channels:
            entity_rating = 0
            subchannels = Channel.objects.filter(parent_id=channel.id)
            # either channel or set of subchannels
            entities = subchannels if len(subchannels) > 0 else [channel]
            for entity in entities:
                content_rating = 0

                contents = Content.objects.filter(channel_id=entity.id)
                for content in contents:
                  rating = 0
                  meta_data = MetaData.objects.filter(content_id=content.id)
                  for data in meta_data:
                    rating += data.rating

                  # avg rating of one content
                  content_rating += rating / len(meta_data) if len(meta_data) > 0 else 0

                entity_rating += content_rating / len(contents) if len(contents) > 0 else 0

            # avg rating of a parent channel
            ratings.append({"Channel Title": channel.title, "Average Rating": f'{entity_rating:.1f}'})

        sorted_ratings = sorted(ratings, key=lambda k: k.get('Average Rating', 0), reverse=True)
        file_name = f"contents/ratings_{int(time.time())}.csv"
        with open(file_name, 'w', encoding='UTF8', newline='') as f:
            writer = csv.DictWriter(f, fieldnames=fieldnames)
            writer.writeheader()
            writer.writerows(sorted_ratings)

            self.stdout.write(file_name)
