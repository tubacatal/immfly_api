import json
from io import StringIO
from django.test import TestCase
from django.core.files.uploadedfile import SimpleUploadedFile
from django.core.management import call_command
from .models import *


class ChannelsTestCases(TestCase):
    def setUp(self):
        image = SimpleUploadedFile(name='immfly.png', content=open(
            'immfly.png', 'rb').read(), content_type='image/png')

        group_1: Group = Group.objects.create(title="test group 1")
        group_2: Group = Group.objects.create(title="test group 2")
        group_3: Group = Group.objects.create(title="test group 3")
        group_4: Group = Group.objects.create(title="test group 4")

        channel_parent_1: Channel = Channel.objects.create(
            title='channel_parent_1', language=Channel.Languages.EN, picture=image)
        channel_parent_1.save()
        channel_parent_1.group.add(group_1, group_2)

        channel_parent_2: Channel = Channel.objects.create(
            title='channel_parent_2', language=Channel.Languages.EN, picture=image)
        channel_parent_2.save()
        channel_parent_2.group.add(group_1, group_2)

        channel_parent_3: Channel = Channel.objects.create(
            title='channel_parent_3', language=Channel.Languages.EN, picture=image)
        channel_parent_3.save()
        channel_parent_3.group.add(group_1)

        channel_subchannel_1: Channel = Channel.objects.create(
            title='channel_subchannel_1', language=Channel.Languages.EN, picture=image,
            parent=channel_parent_1)
        channel_subchannel_1.save()
        channel_subchannel_1.group.add(group_3)

        channel_subchannel_2: Channel = Channel.objects.create(
            title='channel_subchannel_2', language=Channel.Languages.EN, picture=image,
            parent=channel_parent_1)
        channel_subchannel_2.save()
        channel_subchannel_2.group.add(group_3)

        # nested subchannel - won't be added
        channel_subchannel_3: Channel = Channel.objects.create(
            title='channel_subchannel_3', language=Channel.Languages.EN, picture=image,
            parent=channel_subchannel_1)
        channel_subchannel_3.save()
        channel_subchannel_3.group.add(group_4)
        # channel_subchannel_3.group.remove(group_4)

        content_1: Content = Content.objects.create(
            file="contents/immfly.png", channel=channel_parent_2)
        content_1.save()

        content_2: Content = Content.objects.create(
            file="contents/immfly.png", channel=channel_parent_2)
        content_2.save()

        content_3: Content = Content.objects.create(
            file="contents/immfly.png", channel=channel_subchannel_1)
        content_3.save()

        content_4: Content = Content.objects.create(
            file="contents/immfly.png", channel=channel_subchannel_1)
        content_4.save()

        content_5: Content = Content.objects.create(
            file="contents/immfly.png", channel=channel_subchannel_1)
        content_5.save()

        content_6: Content = Content.objects.create(
            file="contents/immfly.png", channel=channel_subchannel_1)
        content_6.save()

        meta_data_1: MetaData = MetaData.objects.create(
            author="test", genre="test", content_description="test", rating=5.5, content=content_1)
        meta_data_1.save()

        meta_data_2: MetaData = MetaData.objects.create(
            author="test", genre="test", content_description="test", rating=9.3, content=content_1)
        meta_data_2.save()

        meta_data_3: MetaData = MetaData.objects.create(
            author="test", genre="test", content_description="test", rating=4.5, content=content_1)
        meta_data_3.save()

        meta_data_4: MetaData = MetaData.objects.create(
            author="test", genre="test", content_description="test", rating=8.6, content=content_2)
        meta_data_4.save()

        meta_data_5: MetaData = MetaData.objects.create(
            author="test", genre="test", content_description="test", rating=7.4, content=content_2)
        meta_data_5.save()

        meta_data_6: MetaData = MetaData.objects.create(
            author="test", genre="test", content_description="test", rating=8.5, content=content_3)
        meta_data_6.save()

        meta_data_7: MetaData = MetaData.objects.create(
            author="test", genre="test", content_description="test", rating=9.6, content=content_4)
        meta_data_7.save()

        meta_data_8: MetaData = MetaData.objects.create(
            author="test", genre="test", content_description="test", rating=3.2, content=content_5)
        meta_data_8.save()

        meta_data_9: MetaData = MetaData.objects.create(
            author="test", genre="test", content_description="test", rating=8.5, content=content_6)
        meta_data_9.save()

        meta_data_10: MetaData = MetaData.objects.create(
            author="test", genre="test", content_description="test", rating=3.5, content=content_6)
        meta_data_10.save()

    def test_rating(self):
        out = StringIO()
        call_command("ratings", stdout=out)
        file_path = out.getvalue().replace("\n", "")

        ratings_data = []
        with open(file_path) as f:
            for row in f:
                ratings_data.append(row.replace("\n", ""))

        formatted_ratings_data = []
        for data in ratings_data[1:]:
            channel, rating = data.split(',')
            formatted_ratings_data.append(
                {"channel": channel, "rating": rating})

        self.assertEqual(formatted_ratings_data, [{'channel': 'channel_parent_2', 'rating': '7.2'},
                                                  {'channel': 'channel_parent_1', 'rating': '6.8'},
                                                  {'channel': 'channel_parent_3', 'rating': '0.0'}])

    # get all parent channels
    def test_get_channels(self):
        response = self.client.get('/channels/')
        data = json.loads(response.content)
        # print(data)
        pass

    # get contents of a parent channel
    def test_get_contents(self):
        response = self.client.get('/channels/2/contents/')
        data = json.loads(response.content)
        # print(data)
        pass

    # get subchannels of a channel
    def test_get_subchannels(self):
      response = self.client.get('/channels/1/subchannels/')
      data = json.loads(response.content)
      # print(data)
      pass

    # get contents of a subchannel
    def test_get_subchannel_contents(self):
      response = self.client.get('/channels/1/subchannels/4/contents/')
      data = json.loads(response.content)
      # print(data)
      pass
