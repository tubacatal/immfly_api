from django.contrib import admin
from .models import Group, Channel, Content, MetaData

admin.site.register(Group)
admin.site.register(Channel)
admin.site.register(Content)
admin.site.register(MetaData)
