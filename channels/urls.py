from django.urls import path
from . import views

urlpatterns = [
    path('', views.get_channels, name='get_all_channels'),
    path('<int:channel_id>/contents/', views.get_contents, name='get_channel_contents'),
    path('<int:channel_id>/subchannels/', views.get_subchannels, name='get_all_subchannels'),
		path('<int:channel_id>/subchannels/<int:subchannel_id>/contents/', views.get_subchannel_contents, name='get_subchannel_contents'),
]