# immfly_api

## Assumptions

If a Channel has a reference to another Channel over the parent field, it means it is a SubChannel.
Nested subchannel creation is not allowed.
A Content can have multiple MetaData entries.

## Further Implementation Notes

1- How to provide different audio & subtitle files to an episode (content)?
Self-reference like channels?
Content type can be added; and content can be collected an sended by the channel_id.
Caution: contents needs to be validated if uploaded file matches the selected content type.

2- When subchannel is deleted, its group should be dropped from the parent's groups.
Caution: groups are sets, so dropped group might belong to another subchannel.

3- Nested subchannel creation error handlingis needed.

4- In case a channel has no subchannels associated within it, it must contain a set of
contents -> requirement is not clear. where to achieve this?
In case of on create, we may add subchannels in following steps?
Can a channel have both subchannels and contents at the same time?
